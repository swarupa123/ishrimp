import logo from './logo.svg';
import './App.css';
import Welcome from './Components/Welcome';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Welcome/>
      </header>
    </div>
  );
}

export default App;
